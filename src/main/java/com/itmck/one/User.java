package com.itmck.one;

import com.itmck.mck.Us;
import com.itmck.spring.annotation.Autowired;
import com.itmck.spring.annotation.Component;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/9/11 18:08
 **/
@Component
public class User implements Us {

    @Override
    public void run() {
        System.out.println("User#run() is running...");
    }
}