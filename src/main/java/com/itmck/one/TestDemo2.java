package com.itmck.one;

import com.itmck.mck.MyConfig;
import com.itmck.mck.UserA;
import com.itmck.mck.UserB;
import com.itmck.spring.core.AnnotationConfigApplicationContext;
import com.itmck.spring.core.AnnotationConfigApplicationContext2;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/24 9:36
 **/
public class TestDemo2 {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MyConfig.class);
        User user = applicationContext.getBean("user", User.class);
        user.run();
    }
}