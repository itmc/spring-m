package com.itmck.mck;

import com.itmck.spring.core.AnnotationConfigApplicationContext3;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/24 9:36
 **/
public class TestDemo3 {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext3 applicationContext = new AnnotationConfigApplicationContext3(MyConfig.class);
        UserA userA = applicationContext.getBean("userA", UserA.class);
        UserB userB1 = userA.getUserB();
        userB1.run();
        UserB userB = applicationContext.getBean("userB", UserB.class);
        UserA userA1 = userB.getUserA();
        userA1.run();
    }
}