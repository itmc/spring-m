package com.itmck.mck;

import com.itmck.spring.core.SmartInstantiationAwareBeanPostProcessor;
import com.itmck.spring.proxy.MyEnhancer;
import com.itmck.spring.proxy.MyProxy;
import net.sf.cglib.proxy.Enhancer;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/9/11 18:08
 **/
public class CglibProxyBeanPostProcessor implements SmartInstantiationAwareBeanPostProcessor {

    @Override
    public Object getEarlyBeanReference(Object bean, String beanName) {
        System.out.println("aop增强:"+beanName);
        if (bean instanceof UserA) {
            MyEnhancer myEnhancer = new MyEnhancer(new MyProxy(),bean);
            Enhancer enhancer = myEnhancer.getEnhancer();
            bean = enhancer.create();
        }
        return bean;
    }


}