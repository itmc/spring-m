package com.itmck.mck;

import com.itmck.spring.annotation.Autowired;
import com.itmck.spring.annotation.Component;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/9/11 18:08
 **/
@Component
public class UserB {

    @Autowired
    private UserA userA;


    public UserA getUserA() {
        return userA;
    }

    public void setUserA(UserA userA) {
        this.userA = userA;
    }

    public void run(){
        System.out.println("UserB");
    }
}