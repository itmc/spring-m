package com.itmck.mck;

import com.itmck.spring.annotation.Autowired;
import com.itmck.spring.annotation.Component;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/9/11 18:08
 **/
@Component
public class UserA{

    @Autowired
    private UserB userB;


    public UserB getUserB() {
        return userB;
    }

    public void setUserB(UserB userB) {
        this.userB = userB;
    }


    public void run() {
        System.out.println("UserA");
    }
}