package com.itmck.mck;

import com.itmck.spring.core.AnnotationConfigApplicationContext2;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/24 9:36
 **/
public class TestDemo {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext2 applicationContext = new AnnotationConfigApplicationContext2(MyConfig.class);
        UserA userA = applicationContext.getBean("userA", UserA.class);
        UserB userB1 = userA.getUserB();
        System.out.println("UserA#userB.run()");
        userB1.run();
        UserB userB = applicationContext.getBean("userB", UserB.class);
        UserA userA1 = userB.getUserA();
        System.out.println("UserB#userA.run()");
        userA1.run();
    }
}