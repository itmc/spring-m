package com.itmck.spring.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 类扫描注解
 *
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/9/11 16:44
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ComponentScan {

    String value() default "";
}