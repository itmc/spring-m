package com.itmck.spring.core;

public interface InitializingBean {
    void afterPropertiesSet();
}