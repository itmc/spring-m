package com.itmck.spring.core;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/9/11 17:28
 **/
public class BeanDefinition {

    /**
     * bean类型
     */
    private Class<?> type;

    /**
     * 是否是单例
     */
    private String scope;

    /**
     * 是否是懒加载
     */
    private boolean isLazy;


    public Class<?> getType() {
        return type;
    }

    public void setType(Class<?> type) {
        this.type = type;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public boolean isLazy() {
        return isLazy;
    }

    public void setLazy(boolean lazy) {
        isLazy = lazy;
    }
}