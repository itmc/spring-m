package com.itmck.spring.core;

@FunctionalInterface
public interface ObjectFactory<T> {

    T getObject();
}