package com.itmck.spring.core;

import com.itmck.spring.annotation.Autowired;
import com.itmck.spring.annotation.Component;
import com.itmck.spring.annotation.ComponentScan;
import com.itmck.spring.annotation.Scope;
import com.itmck.spring.core.*;

import java.beans.Introspector;
import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/24 9:37
 **/
public class MckAnnotationConfigApplicationContext2 {


    /**
     * beanDefinitionMap 存放多个Bean定义 其中key为BeanName,value为BeanDefinition
     */
    private final Map<String, BeanDefinition> beanDefinitionMap = new HashMap<>();

    /**
     * 单例池,如果创建的是单例对象则放入单例池中
     */
    private final Map<String, Object> singletonObjects = new HashMap<>(256);

    //早期Bean,被及早暴露出 用于保存BeanName和Bean实例之间的关系,与singletonObjects不同之处在于,当一个单例Bean被放到这里那么Bean
    //还在创建中,就可以通过getBean获取,当前用来检测循环引用
    private final Map<String, Object> earlySingletonObjects = new ConcurrentHashMap<>(16);

    //Bean工厂,用于保存BeanName与BeanFactory之间的关系
    private final Map<String, ObjectFactory<?>> singletonFactories = new HashMap<>(16);


    /**
     * Names of beans currently excluded from in creation checks.
     */
    private final Set<String> inCreationCheckExclusions = Collections.newSetFromMap(new ConcurrentHashMap<>(16));
    /**
     * 当前正在被创建的单例Bean
     */
    private final Set<String> singletonsCurrentlyInCreation = Collections.newSetFromMap(new ConcurrentHashMap<>(16));

    /**
     * BeanPostProcessor 可以通过BeanPostProcessor提供扩展点,操作Bean定义
     */
    private final List<BeanPostProcessor> beanPostProcessorList = new ArrayList<>();

    /**
     * @param myConfigClass 创建构造函数传入配置类
     */
    public MckAnnotationConfigApplicationContext2(Class<?> myConfigClass) {

        try {
            scanner(myConfigClass);
            refresh();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * 扫描配置类,获取到BeanDefinitionMap
     *
     * @param myConfigClass 配置类
     * @throws Exception 异常
     */
    private void scanner(Class<?> myConfigClass) throws Exception {
        if (myConfigClass.isAnnotationPresent(ComponentScan.class)) {
            //如果当前配置类上存在注解@ComponentScan,则代表当前类为配置类
            ComponentScan componentScan = myConfigClass.getAnnotation(ComponentScan.class);
            //获取value内容  com.itmck.mck
            String path = componentScan.value();
            path = path.replace(".", "/");//将com.itmck.mck-->com/itmck/mck
            //获取当前类加载器
            ClassLoader classLoader = this.getClass().getClassLoader();
            URL resource = classLoader.getResource(path);
            assert resource != null;
            File file = new File(resource.getFile());
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                assert files != null;
                for (File f : files) {
                    String absolutePath = f.getAbsolutePath();
                    absolutePath = absolutePath.substring(absolutePath.indexOf("com"), absolutePath.indexOf(".class"));
                    absolutePath = absolutePath.replace("\\", ".");
                    //通过类加载器,加载当前类信息
                    Class<?> aClass = classLoader.loadClass(absolutePath);
                    if (aClass.isAnnotationPresent(Component.class)) {

                        /**
                         *
                         * isAssignableFrom()方法是判断是否为某个类的父类，
                         * instanceof关键字是判断是否某个类的子类
                         *
                         */

                        if (BeanPostProcessor.class.isAssignableFrom(aClass)) {
                            BeanPostProcessor instance = (BeanPostProcessor) aClass.getConstructor().newInstance();
                            beanPostProcessorList.add(instance);
                        }

                        //如果当前类上存在存在@Component注解
                        Component component = aClass.getAnnotation(Component.class);
                        String beanName = component.value();
                        if ("".equals(beanName)) {
                            //如果没有给默认值,则使用类的首字母小写
                            beanName = Introspector.decapitalize(aClass.getSimpleName());
                        }
                        //扫描的目的就是获取BeanDefinition
                        BeanDefinition beanDefinition = new BeanDefinition();
                        beanDefinition.setType(aClass);
                        //判断是否存在Scope注解标记
                        if (aClass.isAnnotationPresent(Scope.class)) {
                            Scope scope = aClass.getAnnotation(Scope.class);
                            String value = scope.value();
                            beanDefinition.setScope(value);
                        } else {
                            beanDefinition.setScope("singleton");
                        }
                        beanDefinitionMap.put(beanName, beanDefinition);
                    }
                }
            }
        }
    }


    //通过遍历BeanDefinitionMap 创建Bean实例
    private void refresh() {
        //在这里拿到beanDefinitionMap后即可对Bean进行创建
        beanDefinitionMap.forEach((beanName, beanDefinition) -> {
            if ("singleton".equals(beanDefinition.getScope())) {
                Object object = createBean(beanName, beanDefinition);
                singletonObjects.put(beanName, object);
            }
        });


    }

    private Object createBean(String beanName, BeanDefinition beanDefinition) {
        //拿到BeanDefinition创建Bean
        Class<?> aClass = beanDefinition.getType();
        Object instance = null;
        try {
            instance = aClass.getConstructor().newInstance();//无参构造创建Bean
            //Autowired 属性注入
            Field[] declaredFields = aClass.getDeclaredFields();
            for (Field field : declaredFields) {
                if (field.isAnnotationPresent(Autowired.class)) {
                    field.setAccessible(true);
//                    beforeSingletonCreation(field.getName());//针对单例对象,这个用来检测循环依赖的
//                    field.set(instance, getSingleton(field.getName()));
                    field.set(instance, getBean(field.getName()));
                }
            }

            //如果实现了aware回调方法则调用aware回调
            if (instance instanceof BeanNameAware) {
                ((BeanNameAware) instance).setBeanName(beanName);
            }

            //通过BeanPostProcessor堆Bean定义进行扩展
            for (BeanPostProcessor beanPostProcessor : beanPostProcessorList) {
                instance = beanPostProcessor.postProcessBeforeInitialization(instance, beanName);
            }
            //如果当前instance是InitializingBean子类,则调用初始化方法
            if (instance instanceof InitializingBean) {
                ((InitializingBean) instance).afterPropertiesSet();
            }

            //通过BeanPostProcessor堆Bean定义进行扩展
            for (BeanPostProcessor beanPostProcessor : beanPostProcessorList) {
                instance = beanPostProcessor.postProcessAfterInitialization(instance, beanName);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }


    /**
     * 获取单例Bean
     *
     * @param beanName beanName
     * @return
     * @throws Exception
     */
    public Object getSingleton(String beanName) throws Exception {
        return getSingleton(beanName, true);
    }


    /**
     * spring解决循环依赖
     *
     * @param beanName            name
     * @param allowEarlyReference 是否允许循环引用
     * @return 单例Bean
     */
    public Object getSingleton(String beanName, boolean allowEarlyReference) throws Exception {
        //从一级缓存单例池中根据BeanName获取Bean
        Object singletonObject = this.singletonObjects.get(beanName);
        if (singletonObject == null && isSingletonCurrentlyInCreation(beanName)) {
            synchronized (this.singletonObjects) {
                //一级缓存没有取到从二级缓存中取,二级缓存属于早期Bean,Bean还未完全初始化
                singletonObject = this.earlySingletonObjects.get(beanName);
                if (singletonObject == null && allowEarlyReference) {
                    //如果二级缓存没有且存在循环引用,则从三级缓存中取到singletonFactory然后创建
                    ObjectFactory<?> singletonFactory = this.singletonFactories.get(beanName);
                    if (singletonFactory != null) {
                        singletonObject = singletonFactory.getObject();
                        //创建后放入二级缓存,(早期Bean,还不完整)
                        this.earlySingletonObjects.put(beanName, singletonObject);
                        //然后从二级缓存移除singletonFactory
                        this.singletonFactories.remove(beanName);
                    }


                }

            }
        }
        return null;
    }


    //判断当前单例Bean是否正在被创建
    private boolean isSingletonCurrentlyInCreation(String beanName) {
        return singletonsCurrentlyInCreation.contains(beanName);
    }


    /**
     * 针对单例对象,这个用来检测循环依赖的
     * @param beanName
     */
    protected void beforeSingletonCreation(String beanName) {
        if (!this.inCreationCheckExclusions.contains(beanName) && !this.singletonsCurrentlyInCreation.add(beanName)) {
            throw new RuntimeException("循环依赖异常");
        }
    }


    /**
     * 根据BeanName获取对应的Bean实例
     *
     * @param beanName BeanName默认首字母小写
     * @return 获取对应的Bean实例
     */
    public Object getBean(String beanName) {
        if (!beanDefinitionMap.containsKey(beanName)) {
            throw new NullPointerException();
        }
        BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
        //如果是获取的Bean在单例池中存在则从单例池中取,否则新建
        if ("singleton".equals(beanDefinition.getScope())) {
            Object singletonBean = singletonObjects.get(beanName);
            if (singletonBean == null) {
                singletonBean = createBean(beanName, beanDefinition);
                singletonObjects.put(beanName, singletonBean);
            }
            return singletonBean;
        } else {
            // 原型Bean
            return createBean(beanName, beanDefinition);
        }
    }


    public <T> T getBean(String beanName, Class<T> clazz) {

        return clazz.cast(this.getBean(beanName));
    }
}