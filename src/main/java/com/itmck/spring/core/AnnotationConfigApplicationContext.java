package com.itmck.spring.core;

import com.itmck.spring.annotation.Component;
import com.itmck.spring.annotation.ComponentScan;
import com.itmck.spring.annotation.Scope;

import java.beans.Introspector;
import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/25 10:17
 *
 * 模拟spring读取配置文件以及创建Bean,引入以及缓存解决循环依赖
 *
 *
 **/
public class AnnotationConfigApplicationContext {

    /**
     * beanDefinitionMap 存放多个Bean定义 其中key为BeanName,value为BeanDefinition
     */
    private final Map<String, BeanDefinition> beanDefinitionMap = new HashMap<>();

    /**
     * 单例池,如果创建的是单例对象则放入单例池中
     */
    private final Map<String, Object> singletonObjects = new HashMap<>(256);

    /**
     * 通过构造方法传入配置类
     *
     * @param myConfigClass 配置类
     */
    public AnnotationConfigApplicationContext(Class<?> myConfigClass) {

        scan(myConfigClass);
        refresh();
    }

    /**
     * 扫描配置类
     *
     * @param myConfigClass 配置类
     */
    private void scan(Class<?> myConfigClass) {
        if (myConfigClass.isAnnotationPresent(ComponentScan.class)) {
            //如果当前配置类上存在注解@ComponentScan,则代表当前类为配置类
            ComponentScan componentScan = myConfigClass.getAnnotation(ComponentScan.class);
            //获取value内容  com.itmck.mck
            String path = componentScan.value();
            path = path.replace(".", "/");//将com.itmck.mck-->com/itmck/mck
            //获取当前类加载器
            ClassLoader classLoader = this.getClass().getClassLoader();
            URL resource = classLoader.getResource(path);
            assert resource != null;
            File file = new File(resource.getFile());
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                assert files != null;
                for (File f : files) {
                    String absolutePath = f.getAbsolutePath();
                    absolutePath = absolutePath.substring(absolutePath.indexOf("com"), absolutePath.indexOf(".class"));
                    absolutePath = absolutePath.replace("\\", ".");
                    Class<?> aClass = null;
                    try {
                        //通过类加载器,加载当前类信息
                        aClass = classLoader.loadClass(absolutePath);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    assert aClass != null;
                    if (aClass.isAnnotationPresent(Component.class)) {
                        //如果当前类上存在存在@Component注解
                        Component component = aClass.getAnnotation(Component.class);
                        String beanName = component.value();
                        if ("".equals(beanName)) {
                            //如果没有给默认值,则使用类的首字母小写
                            beanName = Introspector.decapitalize(aClass.getSimpleName());
                        }
                        //扫描的目的就是获取BeanDefinition
                        BeanDefinition beanDefinition = new BeanDefinition();
                        beanDefinition.setType(aClass);
                        //判断是否存在Scope注解标记
                        if (aClass.isAnnotationPresent(Scope.class)) {
                            Scope scope = aClass.getAnnotation(Scope.class);
                            String value = scope.value();
                            beanDefinition.setScope(value);
                        } else {
                            beanDefinition.setScope("singleton");
                        }
                        beanDefinitionMap.put(beanName, beanDefinition);
                    }
                }
            }
        }
    }

    /**
     * 创建Bean
     */
    private void refresh() {
        //在这里拿到beanDefinitionMap后即可对Bean进行创建
        beanDefinitionMap.forEach((beanName, beanDefinition) -> {
            if ("singleton".equals(beanDefinition.getScope())) {
                try {
                    Object bean = createBean(beanName, beanDefinition);
                    singletonObjects.put(beanName, bean);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Object createBean(String beanName, BeanDefinition beanDefinition) {
        Class<?> aClass = beanDefinition.getType();
        Object instance = null;
        if (aClass.isAnnotationPresent(Component.class)) {
            try {
                instance = aClass.getConstructor().newInstance();//无参构造创建Bean
                Field[] declaredFields = aClass.getDeclaredFields();
                for (Field field : declaredFields) {
                    String name = field.getName();
                    field.setAccessible(true);
                    field.set(instance, getBean(name));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instance;
    }


    /**
     * 通过BeanName获取Bean对象
     *
     * @param beanName Bean别名
     * @return Bean对象
     */
    public Object getBean(String beanName) {
        if (!beanDefinitionMap.containsKey(beanName)) {
            throw new NullPointerException();
        }
        BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
        //如果是获取的Bean在单例池中存在则从单例池中取,否则新建
        if ("singleton".equals(beanDefinition.getScope())) {
            Object singletonBean = singletonObjects.get(beanName);
            if (singletonBean == null) {
                singletonBean = createBean(beanName, beanDefinition);
                singletonObjects.put(beanName, singletonBean);
            }
            return singletonBean;
        } else {
            // 原型Bean
            return createBean(beanName, beanDefinition);
        }
    }


    /**
     * 通过通过BeanName一级Bean class获取Bean对象
     *
     * @param beanName Bean别名
     * @param tClass   bean class类型
     * @param <T>      实例泛型
     * @return Bean对象
     */
    public <T> T getBean(String beanName, Class<T> tClass) {
        return tClass.cast(getBean(beanName));
    }
}