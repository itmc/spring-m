package com.itmck.spring.core;

public interface BeanNameAware {

    void setBeanName(String name);
}
