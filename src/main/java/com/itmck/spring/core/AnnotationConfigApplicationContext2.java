package com.itmck.spring.core;

import com.itmck.mck.CglibProxyBeanPostProcessor;
import com.itmck.spring.annotation.Component;
import com.itmck.spring.annotation.ComponentScan;
import com.itmck.spring.annotation.Scope;

import java.beans.Introspector;
import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/25 10:17
 *
 * 模拟spring读取配置文件以及创建Bean,引入二级缓存解决多线程同时获取null,以及使用二级缓存解决Aop
 **/
public class AnnotationConfigApplicationContext2 {

    /**
     * beanDefinitionMap 存放多个Bean定义 其中key为BeanName,value为BeanDefinition
     */
    private final Map<String, BeanDefinition> beanDefinitionMap = new HashMap<>();

    /**
     * 单例池,如果创建的是单例对象则放入单例池中
     */
    private final Map<String, Object> singletonObjects = new HashMap<>(256);

    //早期Bean,还未被初始化以及属性赋值
    private final Map<String, Object> earlySingletonObjects = new ConcurrentHashMap<>(16);

    // 三级缓存
    public final Map<String, ObjectFactory<?>> singletonFactories = new ConcurrentHashMap<>();

    /**
     * 通过构造方法传入配置类
     *
     * @param myConfigClass 配置类
     */
    public AnnotationConfigApplicationContext2(Class<?> myConfigClass) {

        scan(myConfigClass);
        refresh();
    }

    /**
     * 扫描配置类
     *
     * @param myConfigClass 配置类
     */
    private void scan(Class<?> myConfigClass) {
        if (myConfigClass.isAnnotationPresent(ComponentScan.class)) {
            //如果当前配置类上存在注解@ComponentScan,则代表当前类为配置类
            ComponentScan componentScan = myConfigClass.getAnnotation(ComponentScan.class);
            //获取value内容  com.itmck.mck
            String path = componentScan.value();
            path = path.replace(".", "/");//将com.itmck.mck-->com/itmck/mck
            //获取当前类加载器
            ClassLoader classLoader = this.getClass().getClassLoader();
            URL resource = classLoader.getResource(path);
            assert resource != null;
            File file = new File(resource.getFile());
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                assert files != null;
                for (File f : files) {
                    String absolutePath = f.getAbsolutePath();
                    absolutePath = absolutePath.substring(absolutePath.indexOf("com"), absolutePath.indexOf(".class"));
                    absolutePath = absolutePath.replace("\\", ".");
                    Class<?> aClass = null;
                    try {
                        //通过类加载器,加载当前类信息
                        aClass = classLoader.loadClass(absolutePath);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    assert aClass != null;
                    if (aClass.isAnnotationPresent(Component.class)) {
                        //如果当前类上存在存在@Component注解
                        Component component = aClass.getAnnotation(Component.class);
                        String beanName = component.value();
                        if ("".equals(beanName)) {
                            //如果没有给默认值,则使用类的首字母小写
                            beanName = Introspector.decapitalize(aClass.getSimpleName());
                        }
                        //扫描的目的就是获取BeanDefinition
                        BeanDefinition beanDefinition = new BeanDefinition();
                        beanDefinition.setType(aClass);
                        //判断是否存在Scope注解标记
                        if (aClass.isAnnotationPresent(Scope.class)) {
                            Scope scope = aClass.getAnnotation(Scope.class);
                            String value = scope.value();
                            beanDefinition.setScope(value);
                        } else {
                            beanDefinition.setScope("singleton");
                        }
                        beanDefinitionMap.put(beanName, beanDefinition);
                    }
                }
            }
        }
    }

    /**
     * 创建Bean
     */
    private void refresh() {
        //在这里拿到beanDefinitionMap后即可对Bean进行创建
        beanDefinitionMap.forEach((beanName, beanDefinition) -> {
            if ("singleton".equals(beanDefinition.getScope())) {
                try {
                    createBean(beanName, beanDefinition);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Object createBean(String beanName, BeanDefinition beanDefinition) {
        //使用一级缓存解决循环依赖
        //在这里先判断一级缓存中是否存在,这里是解决循环依赖的关键,添加一个递归出口,防止无限递归
        Object singleton = getSingleton(beanName);
        if (singleton != null) {
            return singleton;
        }
        Class<?> aClass = beanDefinition.getType();
        Object instance = null;
        if (aClass.isAnnotationPresent(Component.class)) {
            try {
                instance = aClass.getConstructor().newInstance();//无参构造创建Bean
                //在这里直接进行aop不合理,因为没有进行判断,所以要先判断是否需要进行代理,如果需要才进行aop,在哪里判断?
                //当有循环依赖的时候,二级缓存不为null,所以在getSingleton()中判断
                //instance = new CglibProxyBeanPostProcessor().getEarlyBeanReference(instance, beanName);

                earlySingletonObjects.put(beanName, instance);//这里新增二级缓存存放新鲜反射出来的纯净Bean

                Field[] declaredFields = aClass.getDeclaredFields();
                for (Field field : declaredFields) {
                    field.setAccessible(true);
                    field.set(instance, createBean(field.getName(), beanDefinitionMap.get(field.getName())));//在这里调用自身方法进行递归操作获取bean
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        /**
         * 第四步: 初始化
         * 初始化就是设置类的init-method.这个可以设置也可以不设置. 我们这里就不设置了
         */

        //正常的代理实在初始化之后,就是这里进行,但是如果存在循环依赖必须在实例化之后进行
        //instance = new CglibProxyBeanPostProcessor().getEarlyBeanReference(instance, beanName);
        singletonObjects.put(beanName, instance);//将instance放入一级缓存,这里的Bean是完成了属性赋值,初始化等完整的单例Bean
        return instance;
    }

    /**
     * 通过判断
     *
     * @param beanName Bean别名
     * @return Bean对象
     */
    public Object getSingleton(String beanName) {
        if (singletonObjects.containsKey(beanName)) {
            return singletonObjects.get(beanName);
        } else if (earlySingletonObjects.containsKey(beanName)) {
            //新增二级缓存后,一级缓存没有从二级缓存中取
            /**
             * 第一次创建bean是正常的instanceBean. 他并不是循环依赖. 第二次进来判断, 这个bean已经存在了, 就说明是循环依赖了
             * 这时候通过动态代理创建bean. 然后将这个bean在放入到二级缓存中覆盖原来的instanceBean.
             * 这样我们在循环依赖的时候就完成了AOP的创建. 这是在二级缓存里创建的AOP
             */
            Object instance = new CglibProxyBeanPostProcessor().getEarlyBeanReference(earlySingletonObjects.get(beanName), beanName);
            earlySingletonObjects.put(beanName, instance);
            return earlySingletonObjects.get(beanName);
        }
        return null;

    }


    public Object getBean(String beanName) {
        if (!beanDefinitionMap.containsKey(beanName)) {
            throw new NullPointerException();
        }
        BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
        //如果是获取的Bean在单例池中存在则从单例池中取,否则新建
        if ("singleton".equals(beanDefinition.getScope())) {
            Object singletonBean = singletonObjects.get(beanName);
            if (singletonBean == null) {
                singletonBean = createBean(beanName, beanDefinition);
            }
            return singletonBean;
        } else {
            // 原型Bean
            return createBean(beanName, beanDefinition);
        }
    }


    /**
     * 通过通过BeanName一级Bean class获取Bean对象
     *
     * @param beanName Bean别名
     * @param tClass   bean class类型
     * @param <T>      实例泛型
     * @return Bean对象
     */
    public <T> T getBean(String beanName, Class<T> tClass) {
        return tClass.cast(getBean(beanName));
    }
}