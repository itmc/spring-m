package com.itmck.spring.core;


public interface SmartInstantiationAwareBeanPostProcessor extends BeanPostProcessor {


    @Override
    default Object postProcessBeforeInitialization(Object bean, String beanName) {
        return null;
    }

    @Override
    default Object postProcessAfterInitialization(Object bean, String beanName) {
        return null;
    }

    default Object getEarlyBeanReference(Object bean, String beanName) {
        return bean;
    }

}
