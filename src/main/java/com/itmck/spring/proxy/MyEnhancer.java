package com.itmck.spring.proxy;

import net.sf.cglib.proxy.Enhancer;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/25 15:11
 **/
public class MyEnhancer {

    private final MyProxy myProxy;
    private final Object object;

    public MyEnhancer(MyProxy myProxy, Object object) {
        this.myProxy = myProxy;
        this.object = object;
    }

    public Enhancer getEnhancer(){
        Enhancer enhancer = new Enhancer();  // 通过CGLIB动态代理获取代理对象的过程
        enhancer.setSuperclass(object.getClass());     // 设置enhancer对象的父类
        enhancer.setCallback(myProxy);    // 设置enhancer的回调对象
        return enhancer;
    }
}